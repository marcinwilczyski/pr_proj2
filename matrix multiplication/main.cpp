

// Celem tego programu jest prezentacja pomiaru i analizy 
//efektywnosci programu za pomocą  CodeAnalyst(tm).
// Implementacja mnożenia macierzy jest realizowana za pomoca typowego 
// algorytmu podręcznikowego. 
#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "omp.h"
#include <assert.h>
#include <string.h>
#include <vector>

#define USE_MULTIPLE_THREADS true
#define MAXTHREADS 128
#define MAX_SIZE 1000

int NumThreads;
double start;
std::vector< std::pair<std::string, void(*) (float[MAX_SIZE][MAX_SIZE], float[MAX_SIZE][MAX_SIZE], float[MAX_SIZE][MAX_SIZE], int, int)> > functions;



void multiplyMatricesKIJ(float matrixA[MAX_SIZE][MAX_SIZE], float matrixB[MAX_SIZE][MAX_SIZE], float matrixResult[MAX_SIZE][MAX_SIZE], int rows, int columns) {
	for (int k = 0; k < rows; k++)
		for (int i = 0; i < columns; i++)
			for (int j = 0; j < columns; j++)
				matrixResult[i][j] += matrixA[i][k] * matrixB[k][j];
}

void multiplyMatricesKIJompFor(float matrixA[MAX_SIZE][MAX_SIZE], float matrixB[MAX_SIZE][MAX_SIZE], float matrixResult[MAX_SIZE][MAX_SIZE], int rows, int columns) {
#pragma omp parallel for
		for (int k = 0; k < rows; k++)
		for (int i = 0; i < columns; i++)
		for (int j = 0; j < columns; j++)
			matrixResult[i][j] += matrixA[i][k] * matrixB[k][j];
}

void multiplyMatricesKIJompFor2(float matrixA[MAX_SIZE][MAX_SIZE], float matrixB[MAX_SIZE][MAX_SIZE], float matrixResult[MAX_SIZE][MAX_SIZE], int rows, int columns) {
	for (int k = 0; k < rows; k++)
	{
#pragma omp parallel for
		for (int i = 0; i < columns; i++)
		for (int j = 0; j < columns; j++)
			matrixResult[i][j] += matrixA[i][k] * matrixB[k][j];
	}
}

void multiplyMatricesKIJompFor3(float matrixA[MAX_SIZE][MAX_SIZE], float matrixB[MAX_SIZE][MAX_SIZE], float matrixResult[MAX_SIZE][MAX_SIZE], int rows, int columns) {
	for (int k = 0; k < rows; k++)
	for (int i = 0; i < columns; i++)
	{
#pragma omp parallel for
			for (int j = 0; j < columns; j++)
				matrixResult[i][j] += matrixA[i][k] * matrixB[k][j];
	}
}

void createEmptyMatrix(float (&matrix)[MAX_SIZE][MAX_SIZE], int rows, int columns) {
	for (int i = 0; i < rows; i++){
		for (int j = 0; j < columns; j++){
			matrix[i][j] = 0.0f;
		}
	}
}

void destroyMatrix(float matrix[MAX_SIZE][MAX_SIZE], int rows, int columns) {
	for (int i = 0; i < rows; i++){
		delete matrix[i];
	}
	delete matrix;
}

void fillMatrixWithRandomNumbers(float matrix[MAX_SIZE][MAX_SIZE], int rows, int columns) {
	for (int i = 0; i < rows; i++){
		for (int j = 0; j < columns; j++){
			matrix[i][j] = (float)rand() / RAND_MAX;
		}
	}
}

void displayMatrix(float matrix[MAX_SIZE][MAX_SIZE], int rows, int columns) {
	for (int i = 0; i < rows; i++){
		for (int j = 0; j < columns; j++){
			printf("%6.4f ", matrix[i][j]);
		}
		printf("\n");
	}
}

bool compareMatrices(float matrixA[MAX_SIZE][MAX_SIZE], float matrixB[MAX_SIZE][MAX_SIZE], int rows, int columns) {
	for (int i = 0; i < rows; i++){
		for (int j = 0; j < columns; j++){
			if (abs(matrixA[i][j] - matrixB[i][j]) > 0.01f) {
				printf("FAIL: %f %f\n", matrixA[i][j], matrixB[i][j]);
				return false;
			}
		}
	}
	return true;
}

bool testCase1(void(*function) (float[MAX_SIZE][MAX_SIZE], float[MAX_SIZE][MAX_SIZE], float[MAX_SIZE][MAX_SIZE], int, int))
{
	const int rows = 3;
	const int columns = 3;
	float matrixA[MAX_SIZE][MAX_SIZE];
	float matrixB[MAX_SIZE][MAX_SIZE];
	float matrixResult[MAX_SIZE][MAX_SIZE];
	float matrixMultiplicationResult[MAX_SIZE][MAX_SIZE];
	createEmptyMatrix(matrixMultiplicationResult, rows, columns);

	matrixA[0][0] = 0.25f;
	matrixA[0][1] = 1.0f;
	matrixA[0][2] = 0.5f;

	matrixA[1][0] = 0.25f;
	matrixA[1][1] = 0.125f;
	matrixA[1][2] = 0.5f;

	matrixA[2][0] = 1.0f;
	matrixA[2][1] = 0.125f;
	matrixA[2][2] = 0.25f;


	matrixB[0][0] = 1.0f;
	matrixB[0][1] = 0.125f;
	matrixB[0][2] = 0.5f;

	matrixB[1][0] = 0.25f;
	matrixB[1][1] = 0.5f;
	matrixB[1][2] = 0.125f;

	matrixB[2][0] = 1.0f;
	matrixB[2][1] = 0.25f;
	matrixB[2][2] = 0.5f;


	matrixResult[0][0] = 1.0f;
	matrixResult[0][1] = 0.65625f;
	matrixResult[0][2] = 0.5f;

	matrixResult[1][0] = 0.78125f;
	matrixResult[1][1] = 0.21875f;
	matrixResult[1][2] = 0.390625f;

	matrixResult[2][0] = 1.28125f;
	matrixResult[2][1] = 0.25f;
	matrixResult[2][2] = 0.640625f;

	
	function(matrixA, matrixB, matrixMultiplicationResult, rows, columns);

	bool result = compareMatrices(matrixResult, matrixMultiplicationResult, rows, columns);

	return result;
}

bool testCase2(void(*function) (float[MAX_SIZE][MAX_SIZE], float[MAX_SIZE][MAX_SIZE], float[MAX_SIZE][MAX_SIZE], int, int)){
	const int rows = MAX_SIZE;
	const int columns = MAX_SIZE;
	float matrixA[MAX_SIZE][MAX_SIZE];
	float matrixB[MAX_SIZE][MAX_SIZE];
	float matrixResult[MAX_SIZE][MAX_SIZE];
	float matrixMultiplicationResult[MAX_SIZE][MAX_SIZE];
	createEmptyMatrix(matrixMultiplicationResult, rows, columns);
	fillMatrixWithRandomNumbers(matrixA, rows, columns);
	fillMatrixWithRandomNumbers(matrixB, rows, columns);
	multiplyMatricesKIJ(matrixA, matrixB, matrixMultiplicationResult, rows, columns);

	createEmptyMatrix(matrixResult, rows, columns);

	function(matrixA, matrixB, matrixResult, rows, columns);

	bool result = compareMatrices(matrixResult, matrixMultiplicationResult, rows, columns);

	return result;
}

void testUtilFunctions(){
	const int rows = 3;
	const int columns = 3;
	//matrices compare
	float matrixA[MAX_SIZE][MAX_SIZE];
	createEmptyMatrix( matrixA, rows, columns);
	float matrixB[MAX_SIZE][MAX_SIZE];
	createEmptyMatrix(matrixB, rows, columns);
	
	assert(compareMatrices(matrixA, matrixB, rows, columns));

	fillMatrixWithRandomNumbers(matrixA, rows, columns);
	fillMatrixWithRandomNumbers(matrixB, rows, columns);

	assert(!compareMatrices(matrixA, matrixB, rows, columns));

	//matrices multiplication
	for (auto testPair = functions.begin(); testPair != functions.end(); testPair++)
	{
		printf("%s %d\n", testPair->first.c_str(), testCase1(testPair->second));
		printf("%s %d\n", testPair->first.c_str(), testCase2(testPair->second));
	}
}

double testFunction(void(*function) (float[MAX_SIZE][MAX_SIZE], float[MAX_SIZE][MAX_SIZE], float[MAX_SIZE][MAX_SIZE], int, int), const int problemSize) {
	float matrixA[MAX_SIZE][MAX_SIZE];
	createEmptyMatrix(matrixA, problemSize, problemSize);
	float matrixB[MAX_SIZE][MAX_SIZE];
	createEmptyMatrix(matrixB, problemSize, problemSize);
	float matrixResult[MAX_SIZE][MAX_SIZE];
	createEmptyMatrix(matrixResult, problemSize, problemSize);
	fillMatrixWithRandomNumbers(matrixA, problemSize, problemSize);
	fillMatrixWithRandomNumbers(matrixB, problemSize, problemSize);

	double start = (double)clock() / CLK_TCK;
	function(matrixA, matrixB, matrixResult, problemSize, problemSize);
	return (double)clock() / CLK_TCK - start;
}

void addFunctionsToVector(){
	functions.push_back(std::make_pair(std::string("norm"), multiplyMatricesKIJ));
	functions.push_back(std::make_pair(std::string("omp"), multiplyMatricesKIJompFor));
	functions.push_back(std::make_pair(std::string("omp2"), multiplyMatricesKIJompFor2));
	functions.push_back(std::make_pair(std::string("omp3"), multiplyMatricesKIJompFor3));

}

int main(int argc, char* argv[])
{
	addFunctionsToVector();

	if (USE_MULTIPLE_THREADS) {
		SYSTEM_INFO SysInfo;
		GetSystemInfo(&SysInfo);
		NumThreads = SysInfo.dwNumberOfProcessors;
		if(NumThreads > MAXTHREADS)
			NumThreads = MAXTHREADS;
	}
	else
		NumThreads = 1;
	printf("liczba watkow  = %d\n\n", NumThreads);
	testUtilFunctions();
	
	for (auto testPair = functions.begin(); testPair != functions.end(); testPair++)
	{
		printf("KIJ %s\n", testPair->first.c_str());
		printf("Czas: %8.4f sec \n", testFunction(testPair->second, MAX_SIZE));
	}

	getchar();

	return( 0 ) ;
}
